
# blastClust

<!-- badges: start -->
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
<!-- badges: end -->

BlastClust clusters coordinates along a contig as long as they are
distant from less than a threshold distance.

> :book: Documentation: https://hrz-project.gitlab.io/blastclust/ :book:


From a genomic viewpoint, BlastClust clusters together genomic region
where several blast hits of multiple queries were found to co-occur.
Technically, in Bioconductor parlance, BlastClust clusters togethers
ranges of a GenomicRanges object that are distant from less than X bp
into a single range.

<img src="vignettes/fig.png"  width="500">

It can be done on multiple contig (different seqlevels) simultaneously:

<img src="vignettes/fig2.png"  width="500">

## Installation

You can install the released version of blastClust from source with:

``` r
devtools::install_gitlab("hrz-project/blastclust")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(blastClust)
## basic example code
```
