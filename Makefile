# prepare the package for release
PKGNAME := $(shell sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGVERS := $(shell sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGSRC  := $(shell basename `pwd`)

all: check clean

deps:
	# tlmgr install pgf preview xcolor;
	# Rscript -e 'if (!require("Rd2roxygen")) install.packages("Rd2roxygen", repos="http://cran.rstudio.com")'

.PHONY: site
site: public/index.html

public/index.html: vignettes/demonstration.html
	@mkdir -p $(dir $@)
	cp $< $@

.PHONY: docs
docs:
	R -q -e 'devtools::document()'

build: docs
	R CMD build --no-manual --no-build-vignettes .

build-full: build install vignettes
	R CMD build $(PKGSRC)

install-full: build-full
	R CMD INSTALL ./$(PKGNAME)_$(PKGVERS).tar.gz

build-cran:
	R CMD build .

install: build
	R CMD INSTALL ./$(PKGNAME)_$(PKGVERS).tar.gz

check: build-cran
	R CMD check ./$(PKGNAME)_$(PKGVERS).tar.gz --as-cran


examples:

.PHONY: vignettes
vignettes:
	$(MAKE) -C vignettes

clean:
	cd ..;
	$(RM) -r $(PKGNAME).Rcheck/
