
.remove_ov <- function(x) sort(unique(x), ignore.strand = TRUE)

##' classical closure counter
.make_counter <- function() {
  cnt <- 1
  function(inc=1) {
    cnt <<- cnt + inc
    cnt
  }
}

##' ID clusters
##'
##' Return an integer incrementing each time two hits are further
##' appart than \code{thres} base.
##'
##' @return list of integers
.id_clst <- function(d2p, thres) {
  cumsum(d2p > thres) + 1L
}

##' Group hits along a contig
##'
##' Group non-overlapping hits by putting together hits separated by
##' less than a given threshold distance between them. Adds an integer
##' ID denoting the group of hits they belong to.
##'
##' @param unique_hits GRanges, blast hits against *one* contig.
##'
##' @param thres integer, threshold to split two hits in two different
##'   groups.
##'
##' @return GRanges, same dim as unique_hits, with an mcol named
##'   cluster for clustering ID.
.cluster_hits <- function(unique_hits, thres) {
  h <- unique_hits
  if (length(h) <= 1) {
    message(sprintf("Only %d hits found on contig %s, skipping",
                    length(h), unique(seqnames(h))))
    ## return empty granges for endoapply.
    GRanges()
  } else {
    d2f <- distance(
      h[1:length(h) - 1L],
      h[2:length(h)],
      ignore.strand = TRUE)
    x <- list(
      d2p = c(0, d2f),
      d2f = c(d2f, 0)
    )
    h$cluster <- .id_clst(x$d2p, thres)
    h
  }
}

##' Delineate cluster size
##'
##' Summarize the clustered hits by returning their maximal extent.
##'
##' @return GRanges
.delineate_cluster <- function(clustered_hits) {
  ## reduce remove self overlaps and merge to one
  ## say ..|----|.... and
  ##     .....|-----|... get merged to
  ##     ..|--------|...
  S4Vectors::split(clustered_hits, clustered_hits$cluster) %>%
    S4Vectors::endoapply(range, ignore.strand = TRUE) %>%
    unlist() %>%
    GenomicRanges::reduce()
}


##' Call out false positive clusters
##'
##' Remove clusters with either too few hits or narrow extent.
##'
##' @return GRanges, can be empty if none pass the criteria.
.callout_clusters <- function(cluster_ranges,
                              cluster_hits,
                              n_cds_hits = 10,
                              min_cluster_size = 5e3) {
  nov <- countOverlaps(cluster_ranges, cluster_hits)
  rm_clust_few <- cluster_ranges[nov > n_cds_hits]
  ## FIXME maybe add some message here
  rm_clust_small <- rm_clust_few[width(rm_clust_few) > min_cluster_size]
  rm_clust_small
}

##' Cluster blast hits along a contig
##'
##' Given a GRanges object of blast hits, return the maximal extent of
##' hits that are closer together than `max_distance`, with more than `n_cds_hits` found in this region, keeping only regions larger than `min_cluster_size`.
##'
##' The goal is to find hits that are closer than expected, where the
##' 'expected' part is given by the max_distance parameter. As many
##' hits can be spuriously clustered this way without corresponding to
##' a biological reality, a simple way to remove those false positive
##' is to keep only cluster supported by more than `n_cds_hits` hits,
##' and larger than `min_cluster_size`. This seems to work in the case
##' of Cotesia congregata.
##'
##' @return GRanges, with coordinates of detected clusters. Can be empty.
##' @export
cluster_hits_1 <- function(hits,
                           max_distance = 20e3,
                           n_cds_hits = 10,
                           min_cluster_size = 5e3) {
  .dclst <- hits %>%
    .remove_ov() %>%
    .cluster_hits(max_distance)
  .clst_r <- .delineate_cluster(.dclst)
  .callout_clusters(.clst_r, reduce(.dclst, ignore.strand = TRUE),
                    n_cds_hits, min_cluster_size)
}


##' Cluster hits on contigs by distance
##'
##' Basically apply `cluster_hits_1` to all contigs in `raw_hits`,
##' reformating raw blast hits along the way.
##'
##' @param raw_hits GRanges of blast hits (biocblast output)
##'
##' @param ... arguments passed to `cluster_hits_1`
##'
##' @return GRangesList, one element by seqname of raw_hits, describing maximal extent of
cluster_hits <- function(raw_hits, ...) {
  raw_hits %>%
    ## reorder by subject instead of query
    reformat_blast_hits() %>%
    ## keep 10 best blast hits by CDS
    filter_blast_hits() %>%
    split_by_seqnames() %>%
    S4Vectors::endoapply(function(x) {
      tryCatch(cluster_hits_1(x, ...),
               error = function(e) {
                 warning("Error during distance estimation")
                 GRanges()
               })
    })
}

##' Keep only contigs with detected clusters
##'
##' @param clusters_by_contig GRangesList, output of `cluster_hits`.
##'
##' @return GRanges, one row by cluster
keep_contigs_with_cluster <- function(clusters_by_contig) {
  x <- clusters_by_contig
  unlist(x[vapply(x, length, integer(1)) > 0L])
}
